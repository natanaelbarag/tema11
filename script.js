var jsapp = new Vue({
  el: '#color-items',
  data: {
    colors: [],
    title: 'hei',
  },
  methods: {
    getRandomColor: function () {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
      },

    changeColorsHandler: function () {
      let array = [];
      for (let i = 1; i <= 14; i++) {
        array = [...array, this.getRandomColor()];
      }
      this.colors = array;
      console.log({colors: this.colors});

    },

  },

});
